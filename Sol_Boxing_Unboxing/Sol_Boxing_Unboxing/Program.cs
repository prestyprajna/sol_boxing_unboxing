﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Boxing_Unboxing
{
    class Program
    {
        static void Main(string[] args)
        {
            int valueType = 35;

            object obj = valueType;  //boxing -value type to reference type
            Console.WriteLine(obj);

            int val = (int)obj;  //unboxing - reference to value type
            Console.WriteLine(val);
        }
    }
}
